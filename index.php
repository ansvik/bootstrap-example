<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="bootstrap.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript" src="bootstrap.js"></script>
	<script type="text/javascript" src="main.js"></script>
	<title>Site 1</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  		<a class="navbar-brand text-light" href="#">Navbar</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>
  		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    		<div class="navbar-nav">
      			<a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
      			<a class="nav-item nav-link" href="#">Link</a>
      			<a class="nav-item nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
    		</div>
  		</div>
  		<form class="form-inline">
    		<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    		<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  		</form>
	</nav>
	<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
		<ol class="carousel-indicators">
    <li data-target="#carouselExampleFade" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleFade" data-slide-to="1"></li>
    <li data-target="#carouselExampleFade" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="1.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="2.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="3.jpg" class="d-block w-100" alt="...">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg mt-4">
			<div class="text-center">
				<img class="rounded-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
			</div>
			<h3 class="text-center text-dark">Heading</h3>
			<p class="text-secondary">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
			<div class="text-center">
				<button class="btn btn-secondary">View Details >></button>
			</div>
		</div>
		<div class="col-lg mt-4">
			<div class="text-center">
				<img class="rounded-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
			</div>
			<h3 class="text-center text-dark">Heading</h3>
			<p class="text-secondary">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
			<div class="text-center">
				<button class="btn btn-secondary">View Details >></button>
			</div>
		</div>
		<div class="col-lg mt-4">
			<div class="text-center">
				<img class="rounded-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
			</div>
			<h3 class="text-center text-dark">Heading</h3>
			<p class="text-secondary">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
			<div class="text-center">
				<button class="btn btn-secondary">View Details >></button>
			</div>
		</div>
	</div>
	<hr class="featurette-divider">
	<div class="row featurette">
		<div class="col-md-7">
			<h2 class="featurette-heading">
				First featurette heading.
				<span class="text-muted">It'll blow your mind.</span>
			</h2>
			<p class="text-muted ">
				Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.
			</p>
		</div>
		<div class="col-md-5">
			<img class="img-fluid mx-auto" src="500.png">
		</div>
	</div>
	<hr class="featurette-divider">
	<div class="row featurette">
		<div class="col-md-5">
			<img class="img-fluid mx-auto" src="500.png">
		</div>
		<div class="col-md-7">
			<h2 class="featurette-heading">
				Oh yeah, it's that good.
				<span class="text-muted">See for yourself.</span>
			</h2>
			<p class="text-muted ">
				Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.
			</p>
		</div>
	</div>
	<hr class="featurette-divider">
	<div class="row featurette">
		<div class="col-md-7">
			<h2 class="featurette-heading">
				First featurette heading.
				<span class="text-muted">It'll blow your mind.</span>
			</h2>
			<p class="text-muted ">
				Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.
			</p>
		</div>
		<div class="col-md-5">
			<img class="img-fluid mx-auto" src="500.png">
		</div>
	</div>
	<hr class="featurette-divider">
</div>
<footer class="container">
	<p class="float-right">
		<a href="#">Back to top</a>
	</p>
	<p class="mb-5">
		© 2017-2018 Company, Inc. · 
		<a href="#">Privacy</a> ·
		<a href="#">Terms</a>
	</p>
</footer>
</body>
</html>